package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ictrlbk/people/models"
	"gitlab.com/ictrlbk/people/storage"
)

var db storage.Database

func SetDatabase(database storage.Database) {
	db = database
}

func SavePersonHandler(c *gin.Context) {
	var person models.Person
	if err := c.BindJSON(&person); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"success": false,
			"errors":  "invalid JSON format",
		})
		return
	}

	if !isValidIIN(person.IIN) {
		c.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"errors":  "invalid IIN",
		})
		return
	}

	if err := db.SavePerson(&person); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"errors":  "Failed to save person",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"errors":  "",
	})
}

func GetPersonByIINHandler(c *gin.Context) {
	iin := c.Param("iin")

	if !isValidIIN(iin) {
		c.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"errors":  "invalid IIN",
		})
		return
	}

	person, err := db.GetPersonByIIN(iin)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"errors":  "Person not found",
		})
		return
	}

	c.JSON(http.StatusOK, person)
}

func GetPersonByNamePartHandler(c *gin.Context) {
	namePart := c.Param("namePart")

	people, err := db.GetPersonByNamePart(namePart)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"errors":  "Failed to fetch data",
		})
		return
	}

	c.JSON(http.StatusOK, people)
}
