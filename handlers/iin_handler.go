package handlers

import (
	"encoding/json"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type PersonInfo struct {
	Name  string `json:"name"`
	IIN   string `json:"iin"`
	Phone string `json:"phone"`
}

func StorePersonInfo(c *gin.Context) {
	var person PersonInfo

	body, err := io.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"errors":  "failed to read request body",
		})
		return
	}

	if err := json.Unmarshal(body, &person); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"success": false,
			"errors":  "invalid JSON format",
		})
		return
	}

	if !isValidIIN(person.IIN) {
		c.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"errors":  "invalid IIN",
		})
		return
	}

	if err := savePersonInfo(person); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"errors":  err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"errors":  "",
	})
}

func savePersonInfo(person PersonInfo) error {
	data, err := os.ReadFile("people.json")
	if err != nil {
		return err
	}

	var people []PersonInfo
	if err := json.Unmarshal(data, &people); err != nil {
		return err
	}

	people = append(people, person)

	newData, err := json.MarshalIndent(people, "", "  ")
	if err != nil {
		return err
	}

	if err := os.WriteFile("people.json", newData, 0644); err != nil {
		return err
	}

	return nil
}

func CheckIIN(c *gin.Context) {
	iin := c.Param("iin")

	if !isValidIIN(iin) {
		c.JSON(http.StatusBadRequest, gin.H{
			"correct":    false,
			"birth_date": "",
			"sex":        "",
		})
		return
	}

	birthDate, sex := parseIIN(iin)
	c.JSON(http.StatusOK, gin.H{
		"correct":    true,
		"birth_date": birthDate,
		"sex":        sex,
	})
}

func isValidIIN(iin string) bool {
	if len(iin) != 12 || !regexp.MustCompile(`^\d{12}$`).MatchString(iin) {

		return false
	}

	weights := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}
	altWeights := []int{3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2}

	checksum := calculateChecksum(iin, weights)
	if checksum == 10 {
		checksum = calculateChecksum(iin, altWeights)
	}

	validChecksum, _ := strconv.Atoi(string(iin[11]))
	return checksum == validChecksum
}

func calculateChecksum(iin string, weights []int) int {
	sum := 0
	for i, weight := range weights {
		digit, _ := strconv.Atoi(string(iin[i]))
		sum += digit * weight
	}
	return sum % 11
}

func parseIIN(iin string) (string, string) {
	year, _ := strconv.Atoi(iin[:2])
	month, _ := strconv.Atoi(iin[2:4])
	day, _ := strconv.Atoi(iin[4:6])
	centuryCode, _ := strconv.Atoi(string(iin[6]))

	var century int
	switch centuryCode {
	case 1, 2:
		century = 1800
	case 3, 4:
		century = 1900
	case 5, 6:
		century = 2000
	}

	year += century
	birthDate := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC).Format("02.01.2006")

	sex := "male"
	if centuryCode%2 == 0 {
		sex = "female"
	}

	return birthDate, sex
}
