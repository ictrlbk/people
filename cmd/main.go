package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ictrlbk/people/handlers"
	"gitlab.com/ictrlbk/people/storage"
)

func main() {
	router := gin.Default()

	db, err := storage.NewSQLiteDB("people.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	handlers.SetDatabase(db)

	router.GET("/iin_check/:iin", handlers.CheckIIN)
	router.POST("/people/info", handlers.SavePersonHandler)
	router.GET("/people/info/iin/:iin", handlers.GetPersonByIINHandler)
	router.GET("/people/info/phone/:namePart", handlers.GetPersonByNamePartHandler)

	if _, err := db.Exec(`CREATE TABLE IF NOT EXISTS people (
		id INTEGER PRIMARY KEY,
		name TEXT,
		iin TEXT,
		phone TEXT
	)`); err != nil {
		log.Fatal(err)
	}

	log.Println("Server is running on port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
