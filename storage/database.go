package storage

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/ictrlbk/people/models"
)

type Database interface {
	SavePerson(person *models.Person) error
	GetPersonByIIN(iin string) (*models.Person, error)
	GetPersonByNamePart(namePart string) ([]*models.Person, error)
	Exec(query string, args ...interface{}) (sql.Result, error)
	Close() error
}

type SQLiteDB struct {
	*sql.DB
}

func NewSQLiteDB(dataSourceName string) (Database, error) {
	db, err := sql.Open("sqlite3", dataSourceName)
	if err != nil {
		return nil, err
	}
	return &SQLiteDB{db}, nil
}

func (db *SQLiteDB) SavePerson(person *models.Person) error {
	_, err := db.Exec("INSERT INTO people(name, iin, phone) VALUES (?, ?, ?)", person.Name, person.IIN, person.Phone)
	return err
}

func (db *SQLiteDB) GetPersonByIIN(iin string) (*models.Person, error) {
	var person models.Person
	err := db.QueryRow("SELECT name, iin, phone FROM people WHERE iin = ?", iin).Scan(&person.Name, &person.IIN, &person.Phone)
	if err != nil {
		return nil, err
	}
	return &person, nil
}

func (db *SQLiteDB) GetPersonByNamePart(namePart string) ([]*models.Person, error) {
	rows, err := db.Query("SELECT name, iin, phone FROM people WHERE name LIKE ?", "%"+namePart+"%")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var people []*models.Person
	for rows.Next() {
		var person models.Person
		if err := rows.Scan(&person.Name, &person.IIN, &person.Phone); err != nil {
			return nil, err
		}
		people = append(people, &person)
	}

	if len(people) == 0 {
		return []*models.Person{}, nil
	}

	return people, nil
}

func (db *SQLiteDB) Close() error {
	return db.DB.Close()
}
